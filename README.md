# literature


## Background

Dieses Repository beinhaltet die unter dem MeSH "Education" in PubMed gelisteten Publikationen.
Diese wurden außerdem wie folgt gefiltert:

- nur Artikel mit Abstract
- nur Artikel über Menschen

